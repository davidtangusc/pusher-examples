(function() {
	var pusher = new Pusher('19c7a97556307e78234c'); // Replace with your app key
	var channel = pusher.subscribe('news-feed-channel');

	channel.bind('new-post', function(data) {
		console.log(data, data.post);
		$('#posts').prepend('<div class="post">' + data.post + '</div>');
	});

	$('#share-button').on('click', function() {
		var newPost = $('#new-post').val();

		$.ajax({
			url: 'process-new-posts.php',
			type: 'post',
			data: {
				post: newPost
			}
		});
	});

})();



