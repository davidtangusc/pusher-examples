var pusher = new Pusher('19c7a97556307e78234c'); // Replace with your app key
var channel = pusher.subscribe('my-chat-channel');
var user = 'Anonymous';

channel.bind('new-message', function(data) {
	console.log(data);
	var userEl = '<span>' + data.user + '</span>: ';
	var msgEl = '<span>' + data.message + '</span>';
	var el = '<div>' + userEl + msgEl + '</div>';
	$('#msgs').append(el);
});


$('#set-username').on('click', function() {
	user = $('#username').val();
	$('#username-wrapper').html('Username: ' + user);
});


$('#send').on('click', function() {
	var message = $('#message').val();
	$('#message').val('');
	console.log('Message typed: ' + message);

	$.ajax({
		type: 'POST',
		data: {
			user: user,
			message: message
		},
		url: 'process-chats.php',
		success: function() {

		}
	});

});